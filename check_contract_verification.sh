#!/bin/bash

PACT_BROKER=${PACT_BROKER:-"http://localhost:9082"}
deployable=$( curl -s $PACT_BROKER/matrix/provider/Sign%20up%20Service/consumer/Location%20Promotion%20Service | grep 'deployable' | grep true | wc -l | tr -d '[:space:]')

if [[ $deployable == 1 ]]; then
    echo "deployable"
else
    echo "not deployable"
    exit 1
fi
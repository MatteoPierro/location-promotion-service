const mailService = require('./src/service/mailService')
const PromotionService = require('./src/service/locationPromotionService');
const express = require('express')

const signupServiceUrl = process.env.SIGN_UP_SERVICE_URL || "http://localhost:7777"

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const promotionService = new PromotionService(signupServiceUrl, mailService);

app.post('/sendPromotions', (request, response) => {
    const location = request.query.location;
    promotionService.sendPromotionsFor(location);
    response.status(202).end();
});

app.listen('2222', () => {
    console.log('Location Service Up&Running');
})
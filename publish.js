const pact = require('@pact-foundation/pact-node')
const path = require('path')

const pactBrocker = process.env.PACT_BROKER || 'http://localhost:8082'
console.log(`PactBrocker ${pactBrocker}`)
const opts = {
    pactFilesOrDirs: [path.resolve(__dirname, './pacts')],
    pactBroker: pactBrocker,
    tags: ['prod', 'test'],
    consumerVersion: '1.0.0'
}

pact.publishPacts(opts)
    .then(() => {
        console.log('Pact contract publishing complete!')
        console.log('')
        console.log(`Head over to ${pactBrocker}`)
        console.log('to see your published contracts.')
    })
    .catch(e => {
        console.log('Pact contract publishing failed: ', e)
    })
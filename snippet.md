# Location Promotion Service

## Test

### 1 - setup the provider

```javascript
const provider = new Pact({
        consumer: 'Location Promotion Service',
        provider: 'Sign up Service',
        port: MOCK_SERVER_PORT,
        log: path.resolve(process.cwd(), 'logs', 'mockserver-integration.log'),
        dir: path.resolve(process.cwd(), 'pacts'),
        logLevel: LOG_LEVEL,
        spec: 2
    });
```

### 2 - instantiate the system under test

```javascript
const signUpServiceUrl = `localhost:${MOCK_SERVER_PORT}`
const locationPromotionService = new LocationPromotionService(signUpServiceUrl, mailService);
```

### 3 - Stub mail service + before .... after

```javascript
const stubbedMailService = sinon.stub(mailService, 'send');
    before(() => provider.setup())

afterEach(() => {
    stubbedMailService.reset();
    return provider.verify();
});

after(() => provider.finalize())
```

### 4 - add interaction with the mock server

```javascript
before(() => {
        const interaction = {
            state: 'state description here',
            uponReceiving: 'a request for foos',
            withRequest: {
                method: 'OPTIONS',
                path: '/api/foos',
                query: { bar: "something" }
            },
            willRespondWith: {
                status: 999,
                headers: {
                    'Content-Type': '???'
                },
                body: "your body :)"
            }
        }
        return provider.addInteraction(interaction)
    })
```

### 5 - add test

```javascript
xit("should do something", () => {
    return locationPromotionService
                .sendPromotionsFor('???')
                .then( () => {
                    expect(false).to.be.equal(true);
                })
    });
```

#### JSON Content Type

```text
application/json; charset=utf-8
```

#### Sinon expectations

```javascript
const foo = {};
foo.execute = (bar) =>  { console.log(bar.value)};
const stubbedFoo = sinon.stub(foo, 'execute');
foo.execute({value: "fizz"});
expect(foo.callCount).to.be.equal(1);
expect(foo.firstCall.lastArg.value).equal('fizz');
stubbedFoo.reset()
```

## Implementation

### Send an email

```javascript
const mailService = require('./mailService')

mailService.send({
    to: 'an@email.com',
    subject: 'Greetings',
    text: 'Hello dude!'
});
```

### Send a GET with superagent

```javascript
return request
    .get(`localhost/api/foos?bar=something`)
    .then((response) => {
        const foos = response.body;
        // Do something useful with foos :)
    }

```
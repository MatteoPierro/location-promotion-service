const request = require('superagent')

const LocationPromotionService = function(signUpServiceUrl, mailService){
    this.signUpServiceUrl = signUpServiceUrl;
    this.mailService = mailService;
}

LocationPromotionService.prototype.sendPromotionsFor = function(location) {
    return Promise.reject('You should implement the LocationPromotionService')
}

module.exports = LocationPromotionService

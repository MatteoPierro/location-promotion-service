const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const Base64 = require('js-base64').Base64;
const MailComposer = require('nodemailer/lib/mail-composer');

const SCOPES = ['https://www.googleapis.com/auth/gmail.send'];
const TOKEN_PATH = 'secret/token.json';
const CREDENTIALS_PATH = 'secret/credentials.json'

function sendEmail(email) {  
  fs.readFile(CREDENTIALS_PATH, (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    authorize(JSON.parse(content), send(email));
  });
}

function authorize(credentials, callback) {
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0]);

  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

function send(email) {
  return (auth) => {
    const gmail = google.gmail({ version: 'v1', auth });
    const mail = new MailComposer(email);
    mail.compile().build((err, message) => {
      if (err) return console.log('MailComposer returned an error: ' + err);
      console.log(`message ${message.toString('utf8')}`)
      gmail.users.messages.send({
        userId: 'me',
        resource: {
          raw: Base64.encodeURI(message.toString('utf8'))
        }
      }, (err, res) => {
        if (err) return console.log('The API returned an error: ' + err);
        console.log(`Server response ${res.status} ${res.statusText}`);
      });
    });
  }
}

module.exports = {
  send: sendEmail
}